import { render } from "@testing-library/react";

import React, {Component} from 'react';

export default class TimeSelect extends Component{

    state = {
        start_time:new Date(),
        end_time:new Date()
    };
    componentDidUpdate(){
        console.log('cdu', this.state.start_time)
    }

   




    handleSelection = (e)=> {
        let currentTime=new Date();

        let timeSelected=e.target.value;
        
        this.setState({
            end_time:currentTime,
            start_time:new Date(currentTime-(timeSelected*60*60*1000))
        })
        

    };

    render(){
        return <div><select onChange={(e) => this.handleSelection(e)}  style={{width:"100%"}} id="time">
            <option value={1}> 1 Hour</option>
            <option value={2}>2 Hours</option>
            <option value={12}>12 Hours</option>
            <option value={24}>1 Day</option>
            <option value={48}>2 Days</option>
            <option value={72}>3 Days</option>
            <option value={168}>1 Week</option>
            <option value={360}>15 Days</option>
            <option value={720}>1 Month</option>
        </select></div>

    }


}


