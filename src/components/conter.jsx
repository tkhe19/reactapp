import React, {Component} from 'react';

export default class Counter extends Component{
    state = {
        value:0
        
    };

    
     


    subtract= () => {
        const{value}=this.state;
        this.setState({
            value:value-1
        })
    };


    add = () => {
        // const {value} = this.state
        this.setState({
            value: this.state.value + 1
        })


    };
    render(){
        const disableButton=()=>{ return (this.state.value<=0)}
        

        return <div>
        <h1> {this.state.value}</h1>
        <button onClick={this.add}>Add</button>
        <button onClick={this.subtract} disabled={disableButton()}>Subtract</button>
        {/* <p>{this.state.value}</p> */}

        </div>
    }




}